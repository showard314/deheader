/*
 * Items: clock(
 * Standardized-By: SuS
 * Detected-by: gcc-4.4.3 + Linux
 */

#include <time.h>

main(int arg, char **argv)
{
    (void)clock();
}
