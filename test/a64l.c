/*
 * Items: a64l(, l64a(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <stdlib.h>

main(int arg, char **argv)
{
    (void) a64l("2317");
    (void) l64a(2317);

}
