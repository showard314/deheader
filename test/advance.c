/*
 * Items: advance(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#define INIT 
#define GETC() getc()
#define PEEKC() peek()
#define UNGETC(c) ungetc(c)
#define RETURN(ptr) return ptr
#define ERROR(val) return val

#include <regexp.h>

main(int arg, char **argv)
{
    advance("foobar", 0);
}
